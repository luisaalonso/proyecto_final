import numpy as np
#Tomando el codigo aportado por el prof como solucion para Tarea#2, modifico solamente que pongo de parametros los steps iniciales y finales

# Funcion para cargar los datos de una lista de archivos
# Asume que la lista contiene nombres de archivos de la forma
# ##_step.csv donde ## es un int
# Devuelve una tupla: el primer elemento sera la lista ordenada
# de los numeros de steps, el segundo un diccionario que contiene
# las coordenadas x, y, z para cada archivo
def cargar_datos(ruta, lista_archivos, inicio, fin):
    steps = []
    contenidos = {}

    for filename in lista_archivos:
        partes = filename.split('_')
	if int(partes[0]) >= inicio and int(partes[0]) <= fin:
	        indice = int(partes[0])
        	steps.append(indice)
        	# Cargo cada archivo a un array de arrays de numpy
        	datos = np.loadtxt('{dir}/{filename}'.format(dir=ruta, filename=filename), skiprows=1, delimiter=',')
        	# Hago los slices para obtener las coordenadas x, y, z
        	x = datos[:, 0]
        	y = datos[:, 1]
        	z = datos[:, 2]
        	# Guardo los contenidos en memoria. Cada entrada del diccionario
        	# sera un numpy array con las posiciones x, y z y la llave sera
        	# el numero de step
        	contenidos[indice] = np.array([x, y, z])

    # Cuando termino de procesar todos los archivos, ordeno los indices
    steps.sort()
    return steps, contenidos


# Funcion auxiliar para obtener el minimo de las coordenadas x, y, z
def obtener_minimos(contenidos):
    valores = np.array(contenidos.values())
    # valores.shape tiene 3 dimensiones, ej: si eran 128 archivos: (128, 3, 5000)
    min_x = valores[:,0,:].min()
    min_y = valores[:,1,:].min()
    min_z = valores[:,2,:].min()
    return [min_x, min_y, min_z]

# Funcion auxiliar para obtener el maximo de las coordenadas x, y, z
def obtener_maximos(contenidos):
    valores = np.array(contenidos.values())
    # valores.shape tiene 3 dimensiones, ej: si eran 128 archivos: (128, 3, 5000)
    max_x = valores[:,0,:].max()
    max_y = valores[:,1,:].max()
    max_z = valores[:,2,:].max()
    return [max_x, max_y, max_z]
